package com.hendisantika.example.demospringintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoSpringIntegrationApplication {

	public static void main(String[] args) throws InterruptedException {
		ConfigurableApplicationContext ctx = new SpringApplication("integration.xml").run(args);
		Thread.sleep(30000);
		ctx.close();
	}
}
